// Global Variables
let employeeId =localStorage.getItem("employeeId")
let token = localStorage.getItem("token");
let firstName = localStorage.getItem('firstName')
let lastName = localStorage.getItem('lastName')

// Welcome Banner
// let userLoggedIn = document.getElementById('userLoggedIn')
// userLoggedIn.innerHTML = `${firstName} ${lastName}`

// Logout Function
function logoutFunction() {
  localStorage.clear();
  window.location.replace('./logout.html')
}

document.getElementById("logoutButton").onclick = function() {
	logoutFunction();
}


// Sales Order Menu
let SalesOrderMenu = document.getElementById("SalesOrderMenu")

if((localStorage.getItem("isApprover") == "true")) {
  

	SalesOrderMenu.innerHTML = 
	`
  
    <div class="btnContainer">
        <Button class="iconBtn" onclick="salesOrderForApproval()">
          <div class="btnHeader">Sales Order For Approval</div>
            <div class="btn-flex">  
              <img src="./assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
              <div id="count" style="margin-left: 2rem ; margin-top: 1rem">0</div>
            </div>         
        </Button>
    </div>
  `
  

  let count = document.getElementById("count");

  fetch(`https://vast-shelf-95326.herokuapp.com/api/salesOrder/status/allForApprovalStatus`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
	}).then(res => res.json()).then(data => {
    count.innerHTML = data.length
  })
} else {

	SalesOrderMenu.innerHTML =
	`
  <div class="btnContainer">
      <Button class="iconBtn" onclick="salesOrderMonitoring()">
        <div class="btnHeader">Sales Order Monitoring</div>
          <div class="btn-flex">  
            <img src="./assets/image/icons8-purchase-order-60.png" style="margin-top: 1.25rem">
            <div id="count" style="margin-left: 2rem ; margin-top: 1rem">0</div>
          </div>
         
      </Button>
  </div>
  
	`
  let count = document.getElementById("count");
  

  fetch(`https://vast-shelf-95326.herokuapp.com/api/salesOrder/specificRequestor/${employeeId}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		})
		.then(res => res.json())
		.then(data => {
      count.innerHTML = data.length
    }
    )
}

let count = document.getElementById("count");
  

  fetch(`https://vast-shelf-95326.herokuapp.com/api/salesOrder/status/allForApprovalStatus`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		})
		.then(res => res.json())
		.then(data => {
      count.innerHTML = data.length
    }
    )



// let UserMaintenance = document.querySelector("#UserMaintenance")

// if ((localStorage.getItem("isAdmin") == "true")) {

//  let uMmenuContainer = document.querySelector(".uMmenuContainer")

//   uMmenuContainer.innerHTML =  `<a href="#">User Maintenance</a>`

//   UserMaintenance.innerHTML = 
//     `
//     <div class="btnContainer">
//         <Button class="iconBtn" onclick="">
//         <div class="btnHeader">User Maintenance</div>
//         <div>
//           <img src="./assets/image/icons8-purchase-order-60.png">
//         </div>
//         <div class="count">0</div>
//         </Button>
        
//     </div>
  
//     `
//   } else {
//   UserMaintenance.innerHTML = 
  
//   `
  
//   `
// }

function salesOrderCreation(){
location.replace("./salesOrderCreation.html")
}

function salesOrderMonitoring(){
  location.replace("./salesOrderMonitoring.html")
}
function salesOrderForApproval(){
  location.replace("./salesOrderMonitoringFA.html")
}
